# Serializers for our REST framework

from rest_framework import serializers
from .models import UserFavorite    # Model to serialize

class UserFavoriteSerializer( serializers.ModelSerializer  ):
    class Meta:
        model = UserFavorite
        fields = ( "city_id", "city_name"  )