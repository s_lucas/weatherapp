# Services provides the basic calls to remote weather apis, and abstracts away the details of that API
# REST Services provided by openweathermap
# 
# See: https://openweathermap.org/api for further API docs
#
# Weather data is cached in order to reduce outgoing API calls..
#  Weather data cache is by: units_city_id, units_city_name
# 
# NOTE: Any cached data should typically get tossed out to redis or memcached or some
#       other caching server; django spawns processess which will increase memory use as the
#       local service caches grow  
#

import  time, os, json

import requests

from .app_settings import WEATHER_API_KEY
from . import utils


WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/weather?appid={}".format( WEATHER_API_KEY )
WEATHER_API_GROUP_URL = "http://api.openweathermap.org/data/2.5/group?appid={}".format( WEATHER_API_KEY )

VALID_UNITS = ["imperial", "metric"] # Valid units for fetching our weather data; FIRST ELEMENT IS DEFAULT




class CachedWeatherData():
    """
    Cache for weather data; 
    Weather does not change that much over ~5 minute span
    
    Change CACHE_LIFE_TIME to tweak the length of time a cache item is valid
    
    """
    RECENT_WEATHER_CACHE = {}
    CACHE_LIFE_TIME      = 300  # Number of seconds the cache data is considered "valid" (300:5 minutes) 
    
    def __init__(self, cache_key, cache_data):
        self.cache_time = time.time()
        self.cache_key = cache_key
        self.cache_data = cache_data
        
    def is_valid(self):
        return self.cache_time + self.CACHE_LIFE_TIME > time.time()
        
    @classmethod
    def get_cached_data(cls, cache_key):
        """
        Class level function to fetch some cached data
        Returns None if cache key not found OR cache item is invalid
        invalid cache data is popped..
        """
        weather_item = cls.RECENT_WEATHER_CACHE.get(cache_key, None)
        if weather_item != None:
            if weather_item.is_valid(): 
                return weather_item.cache_data
            else:
                cls.RECENT_WEATHER_CACHE.pop( cache_key  )
                
        return None
    
    @classmethod
    def add_cached_data(cls, cache_key, cache_data):
        cd = CachedWeatherData( cache_key, cache_data )
        cls.RECENT_WEATHER_CACHE[cache_key] = cd   
        


def get_weather_by_url_parms(url_parms, units="imperial", apiurl = WEATHER_API_URL):
    """
    Fetch weather data based on out predefined weather url (contains app key )
    
    url_parms: Properly constructed query string to APPEND to WEATHER_API_URL
    
    returns: json object of weather data
    """
    if units not in VALID_UNITS: units = VALID_UNITS[0]    
    
    request_url = "{}&{}&units={}".format( apiurl, url_parms, units)
    
    response_data = requests.get( request_url )
    
    return response_data.json()

def get_weather_data(weather_response_data):
    """
    Returns "interesting" weather data based on the response;
    - "normalizes" the data consumed by the requesting functionality
    
    Out data should be NONE if there's an error
    or be a dictionary with:
    
    city_id: the api's internal method of identifying the particular city / location
    city: Name of city where weather was requested
    temperature: current temp
    description: description of weather
    icon : if icon available from WEATHER_API_URL
    
    """
    outdata = None
    wind_d = weather_response_data.get("wind", {})
    wind_s = wind_d.get("speed", 0)
    wind_deg  = wind_d.get("def", -1)
    
    try:
        outdata = {"city_id"        : weather_response_data["id"],
                   "city"           : weather_response_data["name"],
                   "country"        : weather_response_data["sys"]["country"],
                   "temperature"    : round( weather_response_data["main"]["temp"], 1),
                   "temp_min_max"   : ( round( weather_response_data["main"]["temp_min"], 1),   round( weather_response_data["main"]["temp_max"], 1),    ),
                   "humidity"       : round( weather_response_data["main"]["humidity"], 0),
                   "description"    : weather_response_data["weather"][0]["description"],
                   "icon"           : weather_response_data["weather"][0]["icon"],
                   "wind_speed"     : wind_s,
                   "wind_dir"       : utils.get_wind_dir_from_deg(wind_deg), # Meteorological wind direction
                   }
    except:
        outdata = None
            
    return outdata
        

def get_weather_by_city_ids(city_id_list, units="imperial"):
    """
    Fetch data for a list of city ids
    """
    if units not in VALID_UNITS: units = VALID_UNITS[0]    
    
    cached_data = []    # Hold our cached values
    need_lookups = []
    cache_key_val = "by_id_{}_{}"
    
    # Check the cache for city ids
    for city_id in city_id_list:
        cache_key = cache_key_val.format(city_id, units)
        check_data = CachedWeatherData.get_cached_data(cache_key)
        if check_data != None:
            cached_data.append(check_data)
        else:
            need_lookups.append(city_id)
            
    # Got cities to lookup...
    if len(need_lookups) > 0:
        id_list = ",".join( need_lookups )
        data = get_weather_by_url_parms( "id={}".format( id_list  ), apiurl=WEATHER_API_GROUP_URL, units=units  )
        
        for item in data["list"]:
            cache_key = cache_key_val.format(item["id"], units)
            CachedWeatherData.add_cached_data(cache_key, item)
            cached_data.append(item)
        
    # Now transform the list into consumable format    
    outdata=[]
    for item in cached_data:
        proper_data = get_weather_data(item)
        outdata.append(proper_data)
        
    return outdata
        
    
def get_weather_by_city_id(city_id, units="imperial"):
    """
    Convenience function to fetch data by city-id
    """
    if units not in VALID_UNITS: units = VALID_UNITS[0]    
    
    # Get city data by city ID
    # It will be cached by id and units 
    cache_key = "by_id_{}_{}".format(  city_id, units )
    data = CachedWeatherData.get_cached_data(cache_key)
    if data == None:
        data = get_weather_by_url_parms( "id={}".format( city_id  ), units=units   )
        CachedWeatherData.add_cached_data(cache_key, data)
        
    return get_weather_data( data )


def get_weather_by_zipcode(zipcode, country="us", units="imperial"):
    """
    Convenience function to fetch data by zipcode
    """
    if units not in VALID_UNITS: units = VALID_UNITS[0]    
    cache_key = "by_zip_{}_{}_{}".format(  zipcode, country, units )
    data = CachedWeatherData.get_cached_data(cache_key)
    if data == None:
        data = get_weather_by_url_parms( "zip={},{}".format( zipcode, country  ),units=units   )
        CachedWeatherData.add_cached_data(cache_key, data)
        
    return get_weather_data( data )
    
def get_weather_by_cityname(cityname, country="us",units="imperial"):
    """
    Convenience function to fetch data by city name
    """
    if units not in VALID_UNITS: units = VALID_UNITS[0]    
    cache_key = "by_name_{}_{}_{}".format(  cityname, country, units )
    data = CachedWeatherData.get_cached_data(cache_key)
    if data == None:
        data = get_weather_by_url_parms( "q={},{}".format( cityname, country  ), units=units   )
        CachedWeatherData.add_cached_data(cache_key, data)
        
    return get_weather_data( data )


def get_weather_by_lat_lon(latlon, units="imperial"):
    """
    Convenience function to fetch data by latitude, longitude
    """
    if units not in VALID_UNITS: units = VALID_UNITS[0]
    
    geo_locs = utils.get_geo_locs(latlon)
    if geo_locs == None: return None
    latitude = geo_locs[0]
    longitude = geo_locs[1]
        
    cache_key = "by_ll_{}_{}_{}".format(  latitude, longitude, units )
    data = CachedWeatherData.get_cached_data(cache_key)
    if data == None:
        data = get_weather_by_url_parms( "lat={}&lon={}".format( latitude, longitude  ), units=units   )
        CachedWeatherData.add_cached_data(cache_key, data)
        
        
    return get_weather_data( data )



class CityData():
    
    CITIES_BY_ID = {}   # Cache city data by ID
    CITIES_BY_NAME = {} # LIST of IDS
    
    def __init__(self, json_data=None):
        
        self.id = 0
        self.name = None
        self.country = None
        self.lat = None
        self.lon = None
        
        if json_data != None:
            self.id = json_data["id"]
            self.name = json_data["name"]
            self.country = json_data["country" ]
            self.lat = json_data["coord"]["lat"]
            self.lon = json_data["coord"]["lon"]
            
            # Cache this guy...2 caches for lookups.  One strictly by CITY ID
            # The other by CITY NAME
            
            if self.CITIES_BY_ID.get(self.id, None) == None:
                self.CITIES_BY_ID[ self.id ] = self
                curr_list = self.CITIES_BY_NAME.get(self.name.lower(), None)
                if curr_list == None:
                    curr_list = []
                    self.CITIES_BY_NAME[self.name.lower()] = curr_list
                curr_list.append( self  )
            
    @classmethod           
    def get_cities_by_name(cls, city_name):
        """
        Returns a list of City Objects -- will be necessary to figure out which city a user
        wants to get data for...
        """
        city_name = city_name.lower()
        cities = cls.CITIES_BY_NAME.get(city_name, None)
         
        return cities
    
    @classmethod
    def get_city_by_id(cls, city_id):
        return cls.CITIES_BY_ID.get(city_id, None)
                
            
            
    
        

# Load up the json data
# NOTE: This is better served in some other caching server, such as redis, memcached, etc
#       as django will spin up processes, potentially creating memory issues
path_dir = os.path.dirname(__file__)
cities_path = os.path.join(path_dir, "appdata", "city.list.json")
json_data = None
with open(cities_path, "rt", encoding="utf-8") as f:
    json_data = f.read()
    
if json_data != None:
    city_data = json.loads(json_data, encoding="utf-8")
    for data in city_data:
        item = CityData(data)    
        
json_data = None    

    





