import uuid
from django.db import models
from django.contrib.auth.models import User

from django.db.models.signals import post_save
from django.dispatch import receiver

from .app_settings import MAX_PER_USER_SEARCH_HISTORY
from . import utils


class UserProfile(models.Model):
    """
    Create a user profile model, "extend" the base user model in a nice, relational way
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    default_units = models.CharField(max_length=50, default="imperial") # imperial | metric / -- defaulting to imperial  
    
    
    def __str__(self): return self.user.get_username()
    
# user profiile signal handlers...
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)
        
@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userprofile.save()
    
    
    
    
class UserFavorite(models.Model):
    """
    User Favorites model - store a user's favorites ( "Favorites" Are just the most recently serached items
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)   
    favorite_date   = models.DateTimeField( auto_now_add=True )
    city_name       = models.CharField(max_length=200)          # City Name
    city_id         = models.IntegerField(default=-1)                     # City ID
    
    def __str__(self): return "{}".format(self.city_name) 
    
        
@receiver(post_save, sender=UserFavorite)
def save_user_favorite(sender, instance, **kwargs):
    """
    Limit the max number of Favorites a user can have...
    Deletes oldest entry first
    """
    user = instance.user
    if user.userfavorite_set.count() > MAX_PER_USER_SEARCH_HISTORY:
        data = UserFavorite.objects.filter(user=user).order_by("favorite_date")[0]
        data.delete()    