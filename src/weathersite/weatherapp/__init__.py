from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver


@receiver(user_logged_in)
def on_login(sender, user, request, **kwargs):
    # need to set the request session state to the users default units
    request.session["use_units"] = user.userprofile.default_units
    
     
    