from django.urls import path, include

from rest_framework import routers

import django.contrib.auth.views

from . import views

#from .views import rest


urlpatterns = [path( '', views.index, name="index"  ),
               
               # Signup -- allow a user to create an account that can then be linked to "saved" options
               path ("signup/", views.signup, name="signup"),
               
               # Login -- allow a user to login to their account, to see their saved option
               path ("login/", django.contrib.auth.views.login, {"template_name": "weatherapp/login.html"}, name="login", ),
               
               # On logout, just send user back to "index" page
               path ("logout/", django.contrib.auth.views.logout, {"next_page": "/"}, name="logout", ),
               
               path("units/", views.update_units, name="units_change"),
               
               # Our simple rest-apis
 #              path ("favsapi", views.rest.UserFavoritesList.as_view()),
 #              path ("favsapi/<slug:fid>/", views.rest.UserFavoritesDetail.as_view()),
               
               
               ]

