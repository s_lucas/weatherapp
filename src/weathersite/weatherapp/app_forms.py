from django import forms

class CitySearchForm(forms.Form):
    """
    Basoic search form with a couple fields
    citysearch : What the user inputs
    user_lat_lon : hidden field populated by other means
    """
    citysearch = forms.CharField(label="search", max_length= 100, required=False  )
    user_lat_lon = forms.CharField( max_length= 100, required=False   )
    