

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, RetrieveDestroyAPIView

from ..models import UserFavorite
from ..serializers import UserFavoriteSerializer




# |----------------------------------------------
# | REST API Favorite Views...
# |    Two APIs
# |      - One to List and Create favorites
# |      - One to view details or delete
# |----------------------------------------------
class UserFavoritesList(ListCreateAPIView):
    """
    API Endpoint for listing User Favorite Locations and adding a new one
    -- for ADDING, user must be logged in
    """
    serializer_class = UserFavoriteSerializer
    
    def get_queryset(self):
        user = self.request.user
        data = UserFavorite.objects.filter( user=user  )
        return data

    def get(self, request):
        user = request.user
        # Ensure user has acccess / is authenticated
        if user.is_authenticated == False: return Response({}, status = status.HTTP_403_FORBIDDEN )
        
        return ListCreateAPIView.get(self, request)
    
    def post(self, request):
        # Ensure user has acccess / is authenticated
        user = request.user
        if user.is_authenticated == False: return Response({}, status = status.HTTP_403_FORBIDDEN )
        
        ser = UserFavoriteSerializer( data=request.data )
        if ser.is_valid():
            data = ser.validated_data
            
            city_id = data["city_id"]
            
            city_set = user.userfavorite_set.filter(city_id=city_id)

            if len(city_set) == 0 :
                user.userfavorite_set.create(  city_name = data["city_name"],
                                               city_id   = city_id,
                                             )
            
                return Response( data, status=status.HTTP_201_CREATED  )
            else:
                # Hmm, what to return if there's a dupe?
                return Response( data, status=status.HTTP_200_OK  )
                
        
        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        
    
    
class UserFavoritesDetail(RetrieveDestroyAPIView):
    """
    API Endpoint for User Favorite Locations details (get, delete)
    """    
    def get(self, request, fid=None):
        """
        Fetch a particular  UserFavorite detail on favoite id (uuid)
        """
        # Ensure user has acccess / is authenticated
        user = request.user
        if fid==None or user.is_authenticated == False: return Response({}, status = status.HTTP_403_FORBIDDEN )
        
        try:
            data = UserFavorite.objects.get( user=user, city_id = fid  )
        # Data not found for that particular user/city id combo    
        except UserFavorite.DoesNotExist:
            return Response({}, status = status.HTTP_404_NOT_FOUND )
        # Something else went awry; bad request?!
        except:
            return Response({}, status = status.HTTP_400_BAD_REQUEST )
            
        ser  = UserFavoriteSerializer(data)
        content= ser.data
        return Response(content)

    def delete(self, request,   fid=None):
        """
        Handle the delete verb; remove city_id (based on fid) from the database
        """
        # Ensure user has acccess / is authenticated
        user = request.user
        if fid==None or user.is_authenticated == False: return Response({}, status = status.HTTP_403_FORBIDDEN )
        
        try:
            data = UserFavorite.objects.get( user=user, city_id = fid  )
        # Data not found for that particular user/city id combo    
        except UserFavorite.DoesNotExist:
            return Response({}, status = status.HTTP_404_NOT_FOUND)
        # Something else was wrong with the request -- bad data format?
        except:
            return Response({}, status = status.HTTP_400_BAD_REQUEST)
        
        data.delete(  )
        
        return Response({}, status = status.HTTP_204_NO_CONTENT)