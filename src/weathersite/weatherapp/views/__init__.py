

from django.utils import timezone
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm

from .. import services
from ..app_forms import CitySearchForm
from ..app_settings import GOOGLE_MAP_API_KEY
from .. import utils

INDEX_TEMPLATE_GENERIC          =  "weatherapp/index.html"        
INDEX_TEMPLATE_TOO_MANY_CITIES  =  "weatherapp/index_many_cities.html"

def get_current_city_data_list(user, new_data = None, units="imperial"):
    """
    Grab the current data list for a given user(authenticated),
    if new_data is not none, add it first (or update it's favorite date)
    """
    recent_city_data = None
    if user.is_authenticated:
        # New data -- either create a new record or modify an existing one based on City ID
        if new_data != None:
            city_id     =  new_data["city_id"]
            city_name   =  new_data["city"]
            city_set = user.userfavorite_set.filter(city_id=city_id)
            # No cities with the current id
            if len(city_set) == 0 :
                user.userfavorite_set.create(  city_name = city_name, city_id = city_id)
                
            # Cities with current search city id found, so... update the favorite date    
            else:
                user.userfavorite_set.filter(city_id=city_id).update(favorite_date=timezone.now()  )

        # Update the recent search list
        recent_searches = user.userfavorite_set.all().order_by("-favorite_date")
        
        recent_ids = [str(item.city_id) for item in recent_searches]
        recent_city_data = None
        
        if len(recent_ids) > 0:
            recent_city_data = services.get_weather_by_city_ids(recent_ids, units=units)
            
    return recent_city_data    





def get_city_search_func( search_val ):
    """
    Attempt to determine the proper searching function to use...
    if serach_val is a valid integer, then it's probably a zip code...
     
    """
    if search_val == None: return None
    
    # Straight up integer, attempt to search by zipcode
    if utils.is_valid_int(search_val): 
        return services.get_weather_by_zipcode

    # If there is a comma, separate the string..
    if "," in search_val:
        # Possibly geolocs?
        geo_locs = utils.get_geo_locs(search_val)
        if geo_locs != None: return services.get_weather_by_lat_lon
    
    return None
    

def index(request):
    """
    Index view; handles requests for a user to search for a particular location where
    there is interest in grabbing the weather
    """
    
    template_name = INDEX_TEMPLATE_GENERIC # Used to generate the template; value changed when there are too many cities
    
    context = {"err":None, "weather":None, "city_data_list":None, "map_api_key":GOOGLE_MAP_API_KEY}
    user = request.user

    search_val = None
    search_func = None
    
    units = request.session.get("use_units", "imperial")
    context["units"] = utils.get_unit_descriptors( units )
    
    # Posted to this page; city search time;
    if request.method == "GET":
        # Figure out a search method, if the input data is appropriate
        
        force_to_id = request.session.get("force_weather")
        
        if force_to_id != None:
            search_val = force_to_id
            if utils.is_valid_int(search_val): search_func = services.get_weather_by_city_id
            request.session["force_weather"] = None
            
        if "cityid" in request.GET:
            search_val = request.GET.get("cityid", None)
            if utils.is_valid_int(search_val): search_func = services.get_weather_by_city_id
            
    elif request.method == "POST":
        form = CitySearchForm(request.POST)
        if form.is_valid():
            search_val = form.cleaned_data.get("citysearch")
            # Hidden field used to search by navigator supplied lat/lon coords
            alt_search = form.cleaned_data.get("user_lat_lon")
            if search_val == None or len(search_val) == 0 and alt_search != None and len(alt_search) > 0:
                search_val = alt_search
            
            search_func = get_city_search_func(search_val)
    
    weather_data = None
    # Search value exists and a search function exists
    if search_val != None and search_func != None:
        try:
            weather_data = search_func(search_val, units=units)
            if weather_data == None:
                context["err"] = "Your search resulted in nothing. Try again!"
        except:
            weather_data = None
            context["err"]      = "Something terrible happened! Cannot fetch weather data."
        finally:
            context["weather"] = weather_data
            
    # There's a search value derived from citysearch, and no search function.  So, try to grab a list of cities...    
    elif request.method == "POST" and search_val != None and search_func == None and "citysearch" in request.POST:
        city_list = services.CityData.get_cities_by_name(search_val)
        if city_list == None:
            context["err"] = "No locations match that name. Try again!"
        else:
            template_name = INDEX_TEMPLATE_TOO_MANY_CITIES
            
        context["cities_list"]  = city_list

    # Authenticated user, fetch most recent list (and add new data if it exists)
    if user.is_authenticated:
        recent_city_data = get_current_city_data_list(user, weather_data, units=units)
        # Search data came back empty, if no errors and no weather data, but thre is some city_data_list elements, then
        # set weather to the first (most recently searched) item
        if context["err"] == None and context["weather"] == None and recent_city_data != None and len(recent_city_data) > 0 and context.get("cities_list", None) == None:
            
            context["weather"] = recent_city_data[0]
        context["city_data_list"] = recent_city_data    
    
    request.session["last_viewed_weather"] = context["weather"]
    
    return render(request, template_name, context)



def update_units(request):
    """
    Handler for updating units
    redirects back to index; 
    if there was a recently viewed city, force it to be displayed
    again post redirect
    """
    if request.method == "GET":
        units = request.GET.get("u", None)
        # units is either m or i -- update
        if units in ["m", "i"]:
            new_units  =  utils.get_units(units)
            request.session["use_units"] = new_units 
            if request.user.is_authenticated:
                request.user.userprofile.default_units = new_units
                request.user.userprofile.save()
                
            
    last_weather = request.session.get("last_viewed_weather")
    if last_weather != None: request.session["force_weather"] =last_weather["city_id"] 
    
    return redirect(index)



def signup(request):
    """
    Signup Handler ; creates a users...
    """
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            uname = form.cleaned_data.get("username")
            pword = form.cleaned_data.get("password1")
            user  = authenticate( username=uname, password=pword )
            login(request, user)
            # Grab this user's favorites...
            
            return redirect("index")
    else:
        form = UserCreationForm()
        
    return render( request, "weatherapp/signup.html", {"form":form})






    
