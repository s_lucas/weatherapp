# Application specific settings

# Maximum number of favorites(recent searches) to store
MAX_PER_USER_SEARCH_HISTORY = 8                         

# API KEY provided by the weather service used
WEATHER_API_KEY = "YOUR_KEY_HERE"
    
# API Key for Google Map Selection 
GOOGLE_MAP_API_KEY = "YOUR_KEY_HERE"