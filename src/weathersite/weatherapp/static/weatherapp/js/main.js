
// If geolocation is supported in the browser, then use it to grab the user's current
// location
var default_geo_loc = {lat: 0, lng: 0};
if( navigator.geolocation  )
{
	window.onload = function()
	{
		var geolocation;
		
		function getCurrentGeoLoc(position)
		{
			latitude = position.coords.latitude;
			longitude = position.coords.longitude;
			
			default_geo_loc = {lat: latitude, lng: longitude};
			
			geolocation = latitude + "," + longitude
			// Show the button (it starts our hidden)
			$("#use_user_loc").css("display", "inline");
			// Setup user_loc_click handler
			$("#use_user_loc").click( function(e)
				{
					e.preventDefault();
					$("#user_lat_lon").val( geolocation );
					$("#searchform").submit();
				});

		}
		
		navigator.geolocation.getCurrentPosition(getCurrentGeoLoc);
	}
}

function updateMap(lat, lon)
{
     var uluru =  {lat: lat, lng: lon  };
     var map = new google.maps.Map(document.getElementById('map'), {
       zoom: 4,
       center: uluru
     });
     var marker = new google.maps.Marker({
       position: uluru,
       map: map
     });
}


