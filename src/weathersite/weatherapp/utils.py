import uuid
# Basic utilities used throughout the weatherapp


# Provide a human readable value for meteorological wind directions
WIND_DIRS = ( (348.75,  11.25,  "N"),       # Special case, since it wraps around
              (11.25,   33.75,  "NNE"),
              (33.75,   56.25,  "NE"),
              (56.25,   78.75,  "ENE"),
              (78.75,   101.25, "E"),
              (101.25,  123.75, "ESE"),
              (123.75,  146.25, "SE"),
              (146.25,  168.75, "SSE"),
              (168.75,  191.25, "S"),
              (191.25,  213.75, "SSW"),
              (213.75,  236.25, "SW"),
              (236.25,  258.75, "WSW"),
              (258.75,  281.25, "W"),
              (281.25,  303.75, "WNW"),
              (303.75,  326.25, "NW"),
              (326.25,  348.75, "NNW") )


def get_wind_dir_from_deg(degrees_val):
    """
    Return a human readable string value describing wind direction
    based on the meteorological direction in degrees (0-360)    
    """
    # Not north, so just used 0 and 1 indexes as ranges
    for wind_dir in WIND_DIRS:
        if degrees_val >= wind_dir[0] and degrees_val < wind_dir[1]:
            return wind_dir[2]

    # check north, the wrap-around value
    north = WIND_DIRS[0]
    if degrees_val >= north[0] or degrees_val < north[1]:
        return north[2] 
        
    return "Unknown"    # Uh oh, we should never hit this spot 


def get_uuid_as_str():
    return str(uuid.uuid4())


def is_valid_int(val):
    """
    val is typically a string; use int func to attempt to make an integer
    Returns true if OK, otherwise false
    """
    if val == None: return False
    try:
        int(val)
    except:
        return False
    return True

def is_valid_float(val):
    """
    val is typically a string; use float func to attempt to make an float
    Returns true if OK, otherwise false
    """
    if val == None: return False
    try:
        float(val)
    except:
        return False
    return  True

def get_geo_locs(in_value):
    """
    Take a string and parse it into geo-locations 
    Format of in_value should be something like:   "xxxxxx.xxx, yyyyy.yyy"
    return value is a string tuple or NONE
    """
    # If there is a comma, separate the string..
    if "," in in_value:
        values = in_value.split(",")
        if len(values) == 2:
            v1 = values[0].strip()
            v2 = values[1].strip()
            
            if is_valid_float(v1) and is_valid_float(v2):
                return ( v1,v2  )
            
    return None

def get_units(unit_abv):
    """
    take a unit abbreviation "m" or "i"
    returns "metric" or "imperial"
    
    """
    if unit_abv == "m": 
        return "metric"
    else:
        return "imperial"
    
def get_unit_descriptors(units):
    """
    Return descriptive strings for various measurements for a given set of units
    defaults to imperial
    """
    
    desc = dict(units = "imperial",   speed="mph",    temp ="F")
                
    if units == "metric" or units == "m":
        desc["speed"] = "kph"
        desc["temp"]  = "C"
        desc["units"]  = "metric"
        
    return desc
    
    
