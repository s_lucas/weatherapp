Your Weather
===========
#### Python / Django Weather App

Requirements:
=============
Python 3.6 +
  
Django 2.0 + ( pip install django )
  
Requests frameworkd ( pip install requests )
  
Django REST Framework ( pip install djangorestframework )
  
Notes:
=====
The weather app utilizes the REST API provided by https://openweathermap.org and also utilizes the Google Map API
  
As such, you need to sign up for API keys from both provides. Once your keys are procured, edit weathersite/weatherapp/app_settings.py and
fill replace the "YOUR KEY HERE" values with your new keys.   